package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class AttackDamageTest {
    @Test
    void testIfUnitDoesDamageToOtherUnit(){
        Army testArmy1 = new Army("testArmy1");
        Army testArmy2 = new Army("testArmy2");
        testArmy1.add(new InfantryUnit("Footsoldier", 14));
        testArmy2.add(new CavalryUnit("Knight", 100));

        Battle testBattle = new Battle(testArmy1,testArmy2);
        testBattle.simulate();

        //assertEquals(testArmy1, testBattle.checkWhoWon(testArmy1, testArmy2));


    }
}
