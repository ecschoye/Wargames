package edu.ntnu.idatt2001;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;

/**
 * Class Army
 * @author Edvard Schøyen
 *
 */
public class Army {
    private final String name;
    private ArrayList<Unit> units = new ArrayList<Unit>();

    /**
     * Default constructor
     * @param name
     * @throws IllegalArgumentException
     */
    public Army(String name) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be blank.");
        this.name = name;
    }

    /**
     * Constructor. Takes in a list of units.
     * @param name
     * @param units
     * @throws IllegalArgumentException
     */
    public Army(String name, ArrayList<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be blank.");
        this.name = name;
        this.units = units;
    }

    /**
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a Unit to the list if the list does not contain the unit.
     * @param unit
     */
    public void add(Unit unit){
        if (!units.contains(unit)){
            units.add(unit);
        }
    }

    /**
     * Adds a list of Unit to the units list.
     * @param units
     */
    public void addAll(ArrayList<Unit> units){
        this.units.addAll(units);
    }

    /**
     * Removes a specific unit.
     * @param unit
     */
    public void remove(Unit unit){
            this.units.remove(unit);
    }

    /**
     * Checks if the list of units has Units
     * @return
     */
    public boolean hasUnits(){
        return !units.isEmpty();
    }

    /**
     *
     * @return units
     */
    public ArrayList<Unit> getAllUnits(){
        return this.units;
    }

    /**
     * Returns a random Unit from a list
     * If no Units in list, return null
     * @return Unit
     */
    public Unit getRandom(){
        if (hasUnits()){
            return this.units.get(new Random().nextInt(this.units.size()));
        }
        return null;
    }

    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        String results = ": ";
        for (Unit unit : units){
            results += unit.toString() + " ";
        }
        return "The army: " + name + ", with remaining units " + results;
    }

    /**
     * Checks if two objects are equals
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }

    /**
     *
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
