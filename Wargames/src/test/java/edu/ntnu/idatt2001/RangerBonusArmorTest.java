package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerBonusArmorTest {
    @Test
    void RangerBonusArmorTest(){
        Unit testUnit1 = new RangedUnit("Ranger", 100);
        Unit testUnit2 = new RangedUnit("Ranger", 100);
        assertEquals(6, testUnit2.getResistBonus());
        System.out.println(testUnit2.getResistBonus());
    }

    @Test
    void RangerBonusArmorAfterOneAttackTest(){
        Unit testUnit1 = new RangedUnit("Ranger", 100);
        Unit testUnit2 = new RangedUnit("Ranger", 100);
        testUnit1.attack(testUnit2);
        assertEquals(4, testUnit2.getResistBonus());
        System.out.println(testUnit2.getResistBonus());
    }

    @Test
    void RangerBonusArmorAfterTwoAttacksTest(){
        Unit testUnit1 = new RangedUnit("Ranger", 100);
        Unit testUnit2 = new RangedUnit("Ranger", 100);
        testUnit1.attack(testUnit2);
        testUnit1.attack(testUnit2);
        assertEquals(2, testUnit2.getResistBonus());
        System.out.println(testUnit2.getResistBonus());
    }
}
