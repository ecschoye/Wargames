package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class UnitConstructorTest {
    @Test
    void exceptionUnitTestingWithBlankName(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("", 100));
        assertEquals("Name can not be blank.", exception.getMessage());
    }

    @Test
    void exceptionUnitTestingWithWhitespaceName(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("       ", 100));
        assertEquals("Name can not be blank.", exception.getMessage());
    }

    @Test
    void exceptionUnitTestingWithRegularName(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Footsoldier", 100));
        assertEquals("Name can not be blank.", exception.getMessage());
    }

    @Test
    void exceptionUnitTestingWithNegativeHealth(){
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> new InfantryUnit("Footsoldier", -10));
        assertEquals("Health can not be less than 0.", exception.getMessage());
    }
}
