package edu.ntnu.idatt2001;
/**
 * Class WargamesClient
 * @author Edvard Schøyen
 *
 */
public class WargamesClient {

    public static void main(String[] args) {
        int i;
        Army armyOne = new Army("Human Army");
        Army armyTwo = new Army("Orcish Army");

        for (i = 0; i<=500; i++){
            armyOne.add(new InfantryUnit("Footman", 100));
            armyTwo.add(new InfantryUnit("Grunt", 100));
        }

        for (i = 0; i<=100; i++){
            armyOne.add(new CavalryUnit("Knight", 100));
            armyTwo.add(new CavalryUnit("Raider", 100));
        }
        for (i = 0; i<=200; i++){
            armyOne.add(new RangedUnit("Archer", 100));
            armyTwo.add(new RangedUnit("Spearman", 100));
        }

        armyOne.add(new CommanderUnit("Mountain king", 180));
        armyTwo.add(new CommanderUnit("Gul'dan", 180));

        Battle battle = new Battle(armyOne, armyTwo);
        battle.simulate();

    }
}
