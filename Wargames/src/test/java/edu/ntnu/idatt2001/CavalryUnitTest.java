package edu.ntnu.idatt2001;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CavalryUnitTest {
    @Test

    public void bonusAttackDoesExtraDamage(){
        Unit testUnit = new CavalryUnit("Orc", 25);
        assertTrue(testUnit.getAttackBonus() == 6);
        assertTrue( testUnit.getAttackBonus() == 2);
        assertFalse( testUnit.getAttackBonus() == 6);
    }

    @Test
    void CavalryBonusDamageTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        assertEquals(6, testUnit2.getAttackBonus());
    }

    @Test
    void CavalryBonusDamageAfterOneAttackTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        assertEquals(2, testUnit1.getAttackBonus());
    }

    @Test
    void CavalryBonusDamageAfterTwoAttacksTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        testUnit1.attack(testUnit2);
        assertEquals(2, testUnit1.getAttackBonus());
    }

    @Test
    void CavalryBonusDamageAfterOneLookUpTest(){
        Unit testUnit1 = new CavalryUnit("Knight", 100);
        Unit testUnit2 = new CavalryUnit("Raider", 100);
        testUnit1.attack(testUnit2);
        System.out.println(testUnit1.getAttackBonus());
        assertEquals(2, testUnit1.getAttackBonus());
        System.out.println(testUnit1.getAttackBonus());
    }



}
