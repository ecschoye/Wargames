package edu.ntnu.idatt2001;
/**
 * Class InfantryUnit
 * @author Edvard Schøyen
 *
 */
public class InfantryUnit extends Unit{
    /**
     * Constructor for the Infantry unit
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the Infantry unit
     * @param name
     * @param health
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * Gets attack bonus
     * @return
     */
    @Override
    public int getAttackBonus() {
        return 2;
    }

    /**
     * Gets resist bonus
     * @return
     */
    @Override
    public int getResistBonus() {
        return 2;
    }
}
