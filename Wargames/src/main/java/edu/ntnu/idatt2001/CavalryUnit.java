package edu.ntnu.idatt2001;
/**
 * Class CavalryUnit
 * @author Edvard Schøyen
 *
 */
public class CavalryUnit extends Unit{
    /**
     * Constructor for the Cavalry unit
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the Cavalry unit
     *
     * @param name
     * @param health
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    /**
     * Gets attack bonus
     * increments number of times attacked each time it's attacked
     * if it's been attacked the attack bonus drops from 6 to 2
     * @return int, attack bonus
     */
    @Override
    public int getAttackBonus() {
        this.incrementNumberTimesAttacked();
        return this.getNumberTimesAtt() <= 1 ? 6 : 2;
    }

    /**
     * Gets resist bonus
     * @return int, resist bonus
     */
    @Override
    public int getResistBonus() {
        return 2;
    }
}
