package edu.ntnu.idatt2001;

/**
 * Class Unit
 * @author Edvard Schøyen
 *
 */
public abstract class Unit {
    /**
     * name, health, attack, armor are variables given in the class diagram provided by the exercise
     *
     *
     */
    private final String name;
    private int health;
    private final int attack;
    private final int armor;
    private int numberTimesAtt = 0;

    /**
     * Constructor for Unit class
     * @param name
     * @param health
     * @param attack
     * @param armor
     * @throws IllegalArgumentException
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException{
        if (name.isBlank()) throw new IllegalArgumentException("Name can not be blank.");
        if (health <= 0) throw new IllegalArgumentException("Health can not be less than 0.");
        if (attack < 0) throw new IllegalArgumentException("Attack can not be less than 0");
        if (armor < 0) throw new IllegalArgumentException("Armor can not be less than 0");
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Calculates damage done in an attack
     * @param opponent
     */
    public void attack(Unit opponent) {
       opponent.incrementNumberTimesAttacked();
       opponent.setHealth(opponent.getHealth()
               - (this.getAttack() + this.getAttackBonus())
               + (opponent.getArmor() + opponent.getResistBonus()));
    }


    /**
     * Gets name
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health
     * @return
     */
    public int getHealth() {
        return health;
    }

    /**
     * Increments number of times attacked
     * @return
     */
    public int incrementNumberTimesAttacked(){
        return numberTimesAtt++;
    }

    /**
     * Gets number of times attacked
     * @return
     */
    public int getNumberTimesAtt(){
        return numberTimesAtt;
    }

    /**
     * Gets attack
     * @return
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor
     * @return
     */
    public int getArmor() {
        return armor;
    }

    /**
     * Sets health
     * Sets health to 0 if it's less than 0
     * @param health
     */
    public void setHealth(int health) {
        if (health < 0){
            health = 0;
        }
        this.health = health;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * Gets attack bonus
     * @return
     */
    public abstract int getAttackBonus();

    /**
     * Gets resist bonus
     * @return
     */
    public abstract int getResistBonus();
}
