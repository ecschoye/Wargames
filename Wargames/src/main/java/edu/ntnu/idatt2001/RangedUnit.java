package edu.ntnu.idatt2001;
/**
 * Class RangedUnit
 * @author Edvard Schøyen
 *
 */
public class RangedUnit extends Unit{

    /**
     * Constructor for the Ranged unit
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the Ranged unit
     * @param name
     * @param health
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * Gets attack bonus
     * @return
     */
    @Override
    public int getAttackBonus() {
        return 3;
    }

    /**
     * Gets resist bonus
     * returns 6 if the number of times attacked < 1
     * returns 4 if the number of times attacked < 1
     * else it returns 2
     * @return
     */
    @Override
    public int getResistBonus() {
        if (this.getNumberTimesAtt() < 1){
            return 6;
        }else if (this.getNumberTimesAtt() < 2){
            return 4;
        }else {
            return 2;
        }
    }
}
