package edu.ntnu.idatt2001;
/**
 * Class CommanderUnit
 * @author Edvard Schøyen
 *
 */
public class CommanderUnit extends CavalryUnit {
    /**
     * Constructor for the Commander unit
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor for the Commander unit
     * @param name
     * @param health
     */
    public CommanderUnit(String name, int health) {
        super(name, health,25, 5);
    }

    /**
     * Gets attack bonus
     * @return
     */
    @Override
    public int getAttackBonus() {
        return super.getAttackBonus();
    }

    /**
     * Gets resist bonus
     * @return
     */
    @Override
    public int getResistBonus() {
        return super.getResistBonus();
    }
}
