package edu.ntnu.idatt2001;
/**
 * Class Battle
 * @author Edvard Schøyen
 *
 */
public class Battle {
    Army armyOne;
    Army armyTwo;

    /**
     * Constructor for Battle
     * Takes in armyOne and ArmyTwo as parameters
     * @param armyOne
     * @param armyTwo
     */
    public Battle(Army armyOne, Army armyTwo) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
    }

    /**
     * Simulates war between two armies
     * A random Army gets selected to attack the other Army
     * A random unit from that selected army attacks a random unit from the other army
     * If the attacked units health < 0 it gets removed
     * This goes on until one army is out of units
     * Then checks what army who won
     * @return
     */
    public Army simulate(){
        while (armyOne.hasUnits() && armyTwo.hasUnits()){
            boolean armyOneStrikesFirst = Math.random() < 0.5;
            if (armyOneStrikesFirst){
                Unit striker = armyOne.getRandom();
                Unit defender2 = armyTwo.getRandom();
                battleStats(striker, defender2);
                striker.attack(defender2);
                if (isDead(defender2)){
                    armyTwo.remove(defender2);
                }
            }
            else {
                Unit striker2 = armyTwo.getRandom();
                Unit defender = armyOne.getRandom();
                battleStats(striker2, defender);
                striker2.attack(defender);
                if (isDead(defender)){
                    armyOne.remove(defender);
                }
            }
        }
        checkWhoWon(armyOne, armyTwo);
        return null;
    }

    /**
     *
     * @return String
     */
    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                '}';
    }

    /**
     * Checks which army has units left
     * @param armyOne
     * @param armyTwo
     */
    public static void checkWhoWon(Army armyOne, Army armyTwo){
        StringBuilder stringBuilder = new StringBuilder();
        if (armyOne.hasUnits()){
            stringBuilder.append("\n" + armyOne + " won!");
        }else if(armyTwo.hasUnits()){
            stringBuilder.append("\n" + armyTwo + " won!");
        }else {
            stringBuilder.append("Both armies lost!");
        }
        out(stringBuilder);
    }

    /**
     * adds info about the battle to the stringbuilder
     * @param striker
     * @param defender
     */
    public static void battleStats(Unit striker, Unit defender){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Attacker " + striker + " with " + striker.getHealth() + "hp attacks " + defender + " with " + defender.getHealth() + "hp\n" + defender + " lost " + (striker.getAttack() + striker.getAttackBonus()) + "hp");
        out(stringBuilder);

    }

    /**
     * Checks if the unit is dead
     * adds info about the units health to the stringbuilder
     * @param defender
     * @return
     */
    public static boolean isDead(Unit defender){
        StringBuilder stringBuilder = new StringBuilder();
        if (defender.getHealth() <= 0){
            stringBuilder.append(defender + " has been defeated.");
            out(stringBuilder);
            return true;
        }else {
            stringBuilder.append(defender.getName() + "'s health is now " + defender.getHealth());
            return false;
        }
    }

    /**
     * prints stringbuilder
     * @param stringBuilder
     */
    public static void out(StringBuilder stringBuilder){
        System.out.println(stringBuilder.toString());
    }


}
